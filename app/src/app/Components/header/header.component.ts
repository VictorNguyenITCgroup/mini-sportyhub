import { Component, OnInit } from '@angular/core';
import { SportCenterService } from '../../State/SportCenterState/sport-center.service'
import { SportCenterQuery } from '../../State/SportCenterState/sport-center.query'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(private service:SportCenterService, private query:SportCenterQuery) {
    this.service.get()
  }
  
  ngOnInit(): void {
  }

}
