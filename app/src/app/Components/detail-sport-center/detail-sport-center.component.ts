import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { SportCenterQuery } from '../../State/SportCenterState/sport-center.query'
import { SportCenter } from '../../State/SportCenterState/sport-center.model'

@Component({
  selector: 'app-detail-sport-center',
  templateUrl: './detail-sport-center.component.html',
  styleUrls: ['./detail-sport-center.component.scss']
})
export class DetailSportCenterComponent implements OnInit {

  idDetail:number
  sportDetail:SportCenter
  arrayType

  constructor(route:ActivatedRoute, query:SportCenterQuery  ) { 
    route.params.subscribe(params => { this.idDetail = params['id']; });
    this.sportDetail = query.getEntity(this.idDetail)
    this.arrayType = this.sportDetail.type.split(' - ')
    console.log(this.arrayType);
  }

  ngOnInit(): void {
  }

}
