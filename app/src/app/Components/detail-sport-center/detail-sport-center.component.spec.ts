import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSportCenterComponent } from './detail-sport-center.component';

describe('DetailSportCenterComponent', () => {
  let component: DetailSportCenterComponent;
  let fixture: ComponentFixture<DetailSportCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSportCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSportCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
