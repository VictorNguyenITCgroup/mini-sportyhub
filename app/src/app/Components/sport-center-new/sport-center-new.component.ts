import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SportCenterService } from '../../State/SportCenterState/sport-center.service' 
import { SportCenterQuery } from '../../State/SportCenterState/sport-center.query'
import { classify } from '../helper/typeOfSportCenter'
import { Router } from '@angular/router'


@Component({
  selector: 'app-sport-center-new',
  templateUrl: './sport-center-new.component.html',
  styleUrls: ['./sport-center-new.component.scss']
})
export class SportCenterNewComponent implements OnInit {

  id:number
  checkBanner:string = 'https://file.vforum.vn/hinh/2015/09/hinh-anh-bong-da-3d-2.jpg'
  name = new FormControl('');
  address = new FormControl('');
  banner = new FormControl('');
  phonenumber = new FormControl('');
  type1 = new FormControl('');
  type2 = new FormControl('');
  type3 = new FormControl('');
  type4 = new FormControl('');
  isActive = false

  constructor(private service:SportCenterService, private query:SportCenterQuery, private router: Router ) {}
   
  onSubmit(){
    if(!this.type1.value && !this.type2.value && !this.type3.value && !this.type4.value)
      this.isActive = true      
    else{
      this.query.selectAll().subscribe(data => this.id = data.length + 1) 
      let typeFromForm = `${this.type1.value} ${this.type2.value} ${this.type3.value} ${this.type4.value}`
      let type = classify(typeFromForm)
      if(this.banner.value)
        this.checkBanner = this.banner.value

      let newSportCenter = { "id": this.id, "name":this.name.value, "address":this.address.value, "banner":this.checkBanner, "phonenumber":this.phonenumber.value, "type":type  }
      this.service.add(newSportCenter)
      this.router.navigate(['/home'])
    }
  }
  
  ngOnInit(): void {
  }

}
