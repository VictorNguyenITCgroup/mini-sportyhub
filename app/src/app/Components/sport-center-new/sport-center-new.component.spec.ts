import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportCenterNewComponent } from './sport-center-new.component';

describe('SportCenterNewComponent', () => {
  let component: SportCenterNewComponent;
  let fixture: ComponentFixture<SportCenterNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportCenterNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportCenterNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
