import { Component, OnInit } from '@angular/core'
import { SportCenterService } from '../../State/SportCenterState/sport-center.service'
import { SportCenter } from '../../State/SportCenterState/sport-center.model'
import { SportCenterQuery } from '../../State/SportCenterState/sport-center.query'


@Component({
  selector: 'app-sport-center-list',
  templateUrl: './sport-center-list.component.html',
  styleUrls: ['./sport-center-list.component.scss']
})
export class SportCenterListComponent implements OnInit {
  
  listSportCenter:SportCenter[] 
  listInLocalStore:SportCenter[]

  constructor(private service: SportCenterService, private sportCenterQuery: SportCenterQuery) {  
    this.service.get()
    this.sportCenterQuery.selectAll().subscribe(data => {
      this.listInLocalStore = data.reverse()
      localStorage.setItem("listCenter",JSON.stringify(this.listInLocalStore))
      this.listSportCenter = JSON.parse(localStorage.getItem("listCenter"))
    })
    
  }


  ngOnInit(): void {}

 
  
 

}
