import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportCenterListComponent } from './sport-center-list.component';

describe('SportCenterListComponent', () => {
  let component: SportCenterListComponent;
  let fixture: ComponentFixture<SportCenterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportCenterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportCenterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
