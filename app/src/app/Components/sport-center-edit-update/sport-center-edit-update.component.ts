import { SportCenter } from './../../State/SportCenterState/sport-center.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { FormControl } from '@angular/forms';
import { SportCenterQuery } from '../../State/SportCenterState/sport-center.query'
import { SportCenterService } from '../../State/SportCenterState/sport-center.service'
import { classify } from '../helper/typeOfSportCenter'

@Component({
  selector: 'app-sport-center-edit-update',
  templateUrl: './sport-center-edit-update.component.html',
  styleUrls: ['./sport-center-edit-update.component.scss']
})
export class SportCenterEditUpdateComponent implements OnInit {

  idSportCenter:number
  oldSportCenter:SportCenter
  newSportCenter = {id:0, name:'', address:'', phonenumber:'', banner:'', type:''}

  name = new FormControl('');
  address = new FormControl('');
  banner = new FormControl('');
  phonenumber = new FormControl('');
  type1 = new FormControl('');
  type2 = new FormControl('');
  type3 = new FormControl('');
  type4 = new FormControl('');

  

  constructor(route:ActivatedRoute, query:SportCenterQuery, private service:SportCenterService) { 
    route.params.subscribe(params => { this.idSportCenter = params['id']; });
    this.newSportCenter.id = this.idSportCenter
    this.oldSportCenter = query.getEntity(this.idSportCenter)
  }

  handleClickSave(){
    if(!this.name.value)
      this.newSportCenter.name = this.oldSportCenter.name
    else 
      this.newSportCenter.name = this.name.value
    
    if(!this.address.value)
      this.newSportCenter.address = this.oldSportCenter.address
    else
      this.newSportCenter.address = this.address.value

    if(!this.banner.value)
      this.newSportCenter.banner = this.oldSportCenter.banner
    else
      this.newSportCenter.banner = this.banner.value

    if(!this.phonenumber.value)
      this.newSportCenter.phonenumber = this.oldSportCenter.phonenumber
    else
      this.newSportCenter.phonenumber = this.phonenumber.value

    
    if(!this.type1.value && !this.type2.value && !this.type3.value && !this.type4.value)
      this.newSportCenter.type = this.oldSportCenter.type
    else{
      let typeFromForm = `${this.type1.value} ${this.type2.value} ${this.type3.value} ${this.type4.value}`
      let type = classify(typeFromForm)
      this.newSportCenter.type = type
      console.log(type)
    }
    this.service.update(this.idSportCenter,this.newSportCenter)
  }

  handleClickDelete(){
    this.service.remove(+this.idSportCenter)
  }

  ngOnInit(): void {
  }

}
