import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportCenterEditUpdateComponent } from './sport-center-edit-update.component';

describe('SportCenterEditUpdateComponent', () => {
  let component: SportCenterEditUpdateComponent;
  let fixture: ComponentFixture<SportCenterEditUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportCenterEditUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportCenterEditUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
