import { Injectable } from '@angular/core';
import { SportCenter } from './sport-center.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface SportCenterState extends EntityState<SportCenter> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'sportCenter' })
export class SportCenterStore extends EntityStore<SportCenterState> {

  constructor() {
    super();
  }

}

