import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { SportCenterStore } from './sport-center.store';
import { SportCenter } from './sport-center.model';
import { tap, mapTo } from 'rxjs/operators';
import { Observable, timer } from 'rxjs'
import { SportCenterQuery } from './sport-center.query'



@Injectable({ providedIn: 'root' })
export class SportCenterService {

  constructor(private sportCenterStore: SportCenterStore, private http: HttpClient, private sportCenterQuery:SportCenterQuery) {}

  get(){
    let listInStore = localStorage.getItem('listCenter')
    let arrayList = JSON.parse(listInStore)
    let arrayListSort = arrayList.sort(arrayList.id).reverse()
    this.sportCenterStore.set(arrayListSort)
  }

  add(sportCenter: SportCenter) {
    this.sportCenterStore.add(sportCenter);
  }

  update(id, sportCenter: Partial<SportCenter>) {
    this.sportCenterStore.update(id, sportCenter);
  }

  remove(id: ID) {
    this.sportCenterStore.remove(id);
  }
}
