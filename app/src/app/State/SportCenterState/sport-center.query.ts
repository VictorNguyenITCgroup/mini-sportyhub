import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { SportCenterStore, SportCenterState } from './sport-center.store';

@Injectable({ providedIn: 'root' })
export class SportCenterQuery extends QueryEntity<SportCenterState> {

  constructor(protected store: SportCenterStore) {
    super(store);
  }

}
