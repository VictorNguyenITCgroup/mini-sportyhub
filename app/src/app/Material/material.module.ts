import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';


const componentsOfMaterial = [
  MatGridListModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatCheckboxModule,
  MatInputModule,
  MatFormFieldModule
]

@NgModule({
  imports: [
    CommonModule,
    componentsOfMaterial
  ],
  exports: [ componentsOfMaterial ]
})
export class MaterialModule { }
