import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from '../environments/environment';
import { HeaderComponent } from './Components/header/header.component';
import { MaterialModule } from './Material/material.module';
import { SportCenterListComponent } from './Components/sport-center-list/sport-center-list.component';
import { DetailSportCenterComponent } from './Components/detail-sport-center/detail-sport-center.component';
import { SportCenterNewComponent } from './Components/sport-center-new/sport-center-new.component';
import { SportCenterEditUpdateComponent } from './Components/sport-center-edit-update/sport-center-edit-update.component'
import { AppRoutingModule } from './app-routing.module'
import { FormsModule } from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import { PageNotFoundComponentComponent } from './Components/page-not-found-component/page-not-found-component.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SportCenterListComponent,
    DetailSportCenterComponent,
    SportCenterNewComponent,
    SportCenterEditUpdateComponent,
    PageNotFoundComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    MatRadioModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule.forRoot()
  ],
  providers: [{ provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }}],
  bootstrap: [AppComponent]
})
export class AppModule { }
