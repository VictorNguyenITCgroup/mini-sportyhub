import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailSportCenterComponent } from './Components/detail-sport-center/detail-sport-center.component'
import { SportCenterNewComponent } from './Components/sport-center-new/sport-center-new.component'
import { SportCenterListComponent } from './Components/sport-center-list/sport-center-list.component'
import { SportCenterEditUpdateComponent } from './Components/sport-center-edit-update/sport-center-edit-update.component'
import { PageNotFoundComponentComponent } from './Components/page-not-found-component/page-not-found-component.component'

const routes: Routes = [
  { path:'home', component: SportCenterListComponent, pathMatch:'full'},
  { path:'detail/:id', component: DetailSportCenterComponent },
  { path:'', redirectTo:'home', pathMatch:'full' },
  { path:'new', component: SportCenterNewComponent, pathMatch:'full' },
  { path:'edit/:id', component: SportCenterEditUpdateComponent, pathMatch:'full' },
  { path:'**', component: PageNotFoundComponentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
